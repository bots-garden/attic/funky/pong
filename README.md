# Pong

Pong is a Fiber demo (Golang) application using this container runtime: `registry.gitlab.com/bots-garden/funky/funky-golang-runtime:latest`.

The details of **funky-java-runtime** is here: [https://gitlab.com/bots-garden/funky/funky-golang-runtime/container_registry/](https://gitlab.com/bots-garden/funky/funky-golang-runtime/container_registry/)

## How to deploy your application

```bash
export KUBECONFIG=path_to_your_kubernetes_config_file
export SUB_DOMAIN="X.X.X.X.nip.io" # or whathever you want
./deploy
```

> - you can override the `RUNTIME_IMAGE` variable
>   - the default value is `registry.gitlab.com/bots-garden/funky/funky-golang-runtime:latest`
> - you can override the `NAMESPACE` variable
>   - the default value is `funky-apps-${BRANCH}` where `${BRANCH}` is the current branch

## Before deploying

Edit `install.dependencies.sh` to add the **golang** dependencies you need.

For example:

```bash
go get -u github.com/gofiber/fiber
```